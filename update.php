<?php
include_once "header.php";
include_once "includes/update.inc.php";
if(!logged_in()) {
    redirect("index.php");
}
?>
?>
	<div class="row">
		<div class="col-md-12">
			<?php display_message();
			 ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" method="post" name="update_form">
				<h2>Update User information</h2>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" tabindex="1" value="<?php echo $firstname ;?>" required>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" tabindex="2" value="<?php echo $lastname;?>" required>
						</div>
					</div>
				</div>
				<div class="form-group">
					<input type="text" name="display_name" id="display_name" class="form-control" placeholder="Display Name" tabindex="3" value="<?php echo $displayname;?>" required>
				</div>
				<div class="form-group">
					<select class="form-control" name="usertype">
						<option value="">--select--</option>
						<option value="Admin">Staff</option>
						<option value="Supper_Admin">Admin</option>
					</select>
				</div>
				<div class="form-group">
					<input type="email" name="email" id="email" class="form-control" placeholder="Email Address" tabindex="4" value="<?php echo $email;?>" required>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="password" name="password" id="password" class="form-control" placeholder="Password" tabindex="5" required>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-xs-6 col-md-3"><input type="submit" name="update" value="Update" class="btn btn-primary btn-block " tabindex="7"></div>
					<div class="col-xs-6 col-md-3"><a href="superadmin.php" class="btn btn-success"> Back >></a></div>
				</div>
			</form>
		</div>
	</div>
<script>
	var password = document.getElementById("password")
	, confirm_password = document.getElementById("confirm_password");

	function validatePassword(){
		if(password.value != confirm_password.value) {
			confirm_password.setCustomValidity("Passwords Don't Match");
		} else {
			confirm_password.setCustomValidity('');
		}
	}

	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
</script>
<?php
include_once "footer.php";
?>
