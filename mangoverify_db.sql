-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 15, 2020 at 08:32 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mangoverify_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `usertype` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `first_name`, `last_name`, `display_name`, `usertype`, `email`, `password`, `date_added`) VALUES
(2, 'Nshuti', 'Thierry', 'Nshuti', 'Admin', 'nshuti18@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '2020-04-11 07:11:35'),
(4, 'Alex', 'BW', 'Bwanakweli', 'Staff', 'bwanakweli4ever@gmail.com', '0192023a7bbd73250516f069df18b500', '2020-04-11 15:04:16'),
(15, 'Mango', 'Admin', 'MangoAdmin', 'Admin', 'admin@mango.com', '21232f297a57a5a743894a0e4a801fc3', '2020-04-15 10:25:42');

-- --------------------------------------------------------

--
-- Table structure for table `verified`
--

DROP TABLE IF EXISTS `verified`;
CREATE TABLE IF NOT EXISTS `verified` (
  `verification_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_number` varchar(12) NOT NULL,
  `verified_by` varchar(100) NOT NULL,
  `date_of_verification` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` varchar(60) NOT NULL,
  `sent_otp` int(50) NOT NULL,
  PRIMARY KEY (`verification_id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verified`
--

INSERT INTO `verified` (`verification_id`, `service_number`, `verified_by`, `date_of_verification`, `status`, `sent_otp`) VALUES
(74, '0787490069', 'Administ', '2020-04-13 08:13:46', 'in Progress', 380449),
(70, '0787490069', 'Administ', '2020-04-13 07:40:43', 'verified', 585060),
(71, '0787490069', 'Administ', '2020-04-13 07:44:06', 'verified', 601074),
(72, '0787490069', 'Administ', '2020-04-13 08:11:09', 'verified', 723776),
(73, '0787490069', 'Administ', '2020-04-13 08:12:39', 'verified', 153339);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
