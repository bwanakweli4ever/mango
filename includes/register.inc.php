<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$first_name				= mysqli_real_escape_string($conn,$_POST['first_name']);
	$last_name				= mysqli_real_escape_string($conn,$_POST['last_name']);
	$display_name			= mysqli_real_escape_string($conn,$_POST['display_name']);
	$usertype					= mysqli_real_escape_string($conn,$_POST['usertype']);
	$email						= mysqli_real_escape_string($conn,$_POST['email']);
	$password					= mysqli_real_escape_string($conn,md5($_POST['password']));
	$confirm_password	= mysqli_real_escape_string($conn,$_POST['confirm_password']);

	$errors = [];

	if (email_exists($email))
	{
		$errors[] = "$email is already registered.";
	}

	if (!empty($errors)) {
		foreach ($errors as $error) {
			validation_errors($error);
		}
	}else{
		$sql = "INSERT INTO admins (first_name, last_name, display_name, usertype, email, password, date_added)
		VALUES ('$first_name', '$last_name', '$display_name', '$usertype', '$email', '$password', now())";

		if ($conn->query($sql) === TRUE) {
			redirect("superadmin.php");
			exit;

		} else {
			set_message("<p>Error: " . $sql . "<br>" . $conn->error . "</p>");
		}

		$conn->close();
	}
}
?>
