<?php
include_once "includes/init.php";
$type = get_usertype($_SESSION['email']);

if ($type =='Staff') {
redirect("admin.php");
	}
include_once "header.php";
if(!logged_in()) {
    redirect("index.php");
}
?>
<div class="container">
	<div class="alert alert-success" role="alert">
		<?php echo "Welcome " . get_name($_SESSION['email']); ?>
		<br />
		<?php display_message(); ?>
	</div>
	<br /><br />
<div class="container">

		 <br />
		 <div class="table-responsive">
					<table id="admin_data" class="table table-striped table-bordered table-hover ">
							 <thead>
										<tr>
												 <td>First Name</td>
												 <td>Last Name</td>
												 <td>Display Name</td>
												 <td>User Type</td>
												 <td>Email</td>
												 <td>Date of creation</td>
												 <td>Action</td>
										</tr>
							 </thead>
							 <tbody>
									 <?php
									 $sql= $conn->query("SELECT * FROM admins");
				 						while($data = $sql->fetch_array()){
				 						echo "<tr>";
				 						echo "<td>" . $data['first_name'] . "</td>";
				 						echo "<td>" . $data['last_name'] . "</td>";
				 						echo "<td>" . $data['display_name'] . "</td>";
				 						echo "<td>" . $data['usertype'] . "</td>";
										echo "<td>" . $data['email'] . "</td>";
										echo "<td>" . $data['date_added'] . "</td>";
										echo "<td class='text-left'><a href='update.php?id=".$data['id']."'class='btn btn-success'>Edit</a>
										<a href='delete.php?id=".$data['id']."'class='btn btn-success'>Delete</a> </td>";
				 						echo "</tr>";
				 					}
				 					 ?>
								</tbody>
								<span><a href="create.php" class="btn btn-success" style="margin-bottom:30px;">Add Users</a></span>
					</table>
		 </div>
</div>
</div>
<?php
include_once "footer.php";
?>
<script>
$(document).ready(function(){
$('#admin_data').DataTable();
});
</script>
