<?php
include_once "header.php";

if(!logged_in()) {
    redirect("index.php");
}
?>
					<div class="container">
						<div class="alert alert-success" role="alert" id="demo">
							<?php echo "Welcome " . get_name($_SESSION['email']); ?>
						</div>
							<div class="col-md-3">
								<div class="input-section">
									<h3 class="display1">Verify number</h3>
									<p class="subhead"><small>Type correctly your phone number<span style="color:#e36f28;"><i>(without country code!)</i></span></small></p>
									<br />

										<div id="form-login-username" class="form-group">
											<div class="form-group">
												<input class="form-control tel" type="number" id="phone" name="phone_number" inputmode="tel" value="" maxlength="9"  placeholder="078121437" required="" />
											</div>
										</div>
										<div>
											<button class="btn btn-block ripple-effect" type="submit" name="Submit" alt="sign in" id="myBtn">VERIFY</button>
										</div>

								</div>
							</div>
							<div class="table-section col-md-9">
								<table class="table table-striped table-bordered table-hover sorting_desc" id="mydatatable">
									<thead>
										<tr>
											<th>Index</th>
											<th>Service Number</th>
											<th>OTP</th>
											<th>Verified By</th>
											<th>Date of Verification</th>
											<th>Verification Status</th>
										</tr>
									</thead>
									<tbody class="tbody">
										<?php

										$sql= $conn->query("SELECT * FROM verified ORDER BY verification_id DESC");
										while($data = $sql->fetch_array()){
											echo "<tr>";
											echo "<td>" . $data['verification_id'] . "</td>";
											echo "<td>" . $data['service_number'] . "</td>";
											echo "<td>" . $data['sent_otp'] . "</td>";
											echo "<td>" . $data['verified_by'] . "</td>";
											echo "<td>" . $data['date_of_verification'] . "</td>";
											$pn=$data['service_number'];
											$id=$data['verification_id'];


											if($data['status']=="in Progress"){
											echo "<td>" . "<button class='col-xs-6 verification-btn-Progress'>".$data['status'] ."</button>"."<button type='submit' id='myBtn' class='col-xs-6 verification-btn-resend'><a href='post-verify.php?phone=".$pn."&id=".$id."'>Resend?</button>" ."</td>";
										}
										else{

                                           echo "<td>" . "<button class='verification-btn'>".$data['status'] ."</button>". "</td>";
										}
											echo "</tr>";
										}

										 ?>

										</tbody>
												<tfoot>
													<th>Index</th>
													<th>Service Number</th>
													<th>OTP</th>
													<th>Verified By</th>
													<th>Date of Verification</th>
													<th>Verification Status</th>
													</tr>
												</tfoot>
									</table>
								</div>
						</div>
					<!-- The Modal -->
					<div id="myModal" class="modal">
					  <!-- Modal content -->
					  <div class="modal-content"style="max-width:450px !important;">
							<div class="container">

					    <span class="close">&times;</span>
								<div class="col-md-4" id="entryWindow">
									<h3 class="text-center">Enter the received code</h3>
									 <div id="otp-inputs" class="c-otp__group">
								    <input class="c-otp__input" type="text" placeholder="0" pattern="\d*" inputmode="numeric" autocomplete="one-time-code">
								    <input class="c-otp__input" type="text" placeholder="0" pattern="\d*" inputmode="numeric">
								    <input class="c-otp__input" type="text" placeholder="0" pattern="\d*" inputmode="numeric">
								    <input class="c-otp__input" type="text" placeholder="0" pattern="\d*" inputmode="numeric">
								    <input class="c-otp__input" type="text" placeholder="0" pattern="\d*" inputmode="numeric">
								    <input class="c-otp__input" type="text" placeholder="0" pattern="\d*" inputmode="numeric">
								  </div>
                  <br />
								</div>

								<div class="col-md-4">
									<div id="information"></div>

								</div>
					  </div>
					</div>
					</div>
					</div>






<?php
include_once "footer.php";
?>
<script>
$(document).ready(function(){
$('#mydatatable').DataTable();
});
</script>



<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<script>
//Could put this in into a DB
jQuery(document).ready(function($){

$('#header').text('Enter Email');

function rand() {
		return Math.random().toString(6).substr(2); // remove `0.`
};

function token() {
		return rand() + rand();
};

$('#myBtn').click(function(){
	 var phone=document.getElementById("phone").value;
	 if(phone==""){

	    location.reload();
	 	alert("Service number can not be empty");

	 }
	 else{
    var xhttp = new XMLHttpRequest();
    var phone=document.getElementById("phone").value;
    var user="<?php echo get_name($_SESSION['email']);?>";

  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("demo").innerHTML = this.responseText;
    }
  };
  if(!phone==""){
        <?php $code=substr(str_shuffle(str_repeat('0123456789', '6')), 0, '6'); ?>
     var code="<?php echo $code; ?>";
  xhttp.open("GET", "smsscript.php?number="+phone+"&user=" + user +"&code="+code, true);
  xhttp.send()
}
  } });



// $('#verification_btn').on('submit', function(){
// 	if(!phone==""){
// 	alert("ok");
// }});
});
</script>
<script type="text/javascript">


var BACKSPACE_KEY = 8;
var ENTER_KEY = 13;
var TAB_KEY = 9;
var LEFT_KEY = 37;
var RIGHT_KEY = 39;
var ZERO_KEY = 48;
var NINE_KEY = 57;

function otp(elementId) {
  var inputs = document.getElementById(elementId).children;
  var callback = null;

  function init(completeCallback) {
    callback = completeCallback;
    for (i = 0; i < inputs.length; i++) {
      registerEvents(i, inputs[i]);
    }
  }

  function destroy() {
    for (i = 0; i < inputs.length; i++) {
      registerEvents(i, inputs[i]);
    }
  }

  function registerEvents(index, element) {
    element.addEventListener("input", function (ev) {
      onInput(index, ev);
    });
    element.addEventListener("paste", function (ev) {
      onPaste(index, ev);
    });
    element.addEventListener("keydown", function (ev) {
      onKeyDown(index, ev);
    });
  }

  function onPaste(index, ev) {
    ev.preventDefault();
    var curIndex = index;
    var clipboardData = ev.clipboardData || window.clipboardData;
    var pastedData = clipboardData.getData("Text");
    for (i = 0; i < pastedData.length; i++) {
      if (i < inputs.length) {
        if (!isDigit(pastedData[i])) break;
        inputs[curIndex].value = pastedData[i];
        curIndex++;
      }
    }
    if (curIndex == inputs.length) {
      inputs[curIndex - 1].focus();
      callback(retrieveOTP());
    } else {
      inputs[curIndex].focus();
    }
  }

  function onKeyDown(index, ev) {
    var key = ev.keyCode || ev.which;
    if (key == LEFT_KEY && index > 0) {
      ev.preventDefault(); // prevent cursor to move before digit in input
      inputs[index - 1].focus();
    }
    if (key == RIGHT_KEY && index + 1 < inputs.length) {
      ev.preventDefault();
      inputs[index + 1].focus();
    }
    if (key == BACKSPACE_KEY && index > 0) {
      if (inputs[index].value == "") {
        // Empty and focus previous input and current input is empty
        inputs[index - 1].value = "";
        inputs[index - 1].focus();
      } else {
        inputs[index].value = "";
      }
    }
    if (key == ENTER_KEY) {
      // force submit if enter is pressed
      ev.preventDefault();
      if (isOTPComplete()) {
        callback(retrieveOTP());
      }
    }
    if (key == TAB_KEY && index == inputs.length - 1) {
      // force submit if tab pressed on last input
      ev.preventDefault();
      if (isOTPComplete()) {
        callback(retrieveOTP());
      }
    }
  }

  function onInput(index, ev) {
    var value = ev.data || ev.target.value;
    var curIndex = index;
    for (i = 0; i < value.length; i++) {
      if (i < inputs.length) {
        if (!isDigit(value[i])) {
          inputs[curIndex].value = "";
          break;
        }
        inputs[curIndex++].value = value[i];
        if (curIndex == inputs.length) {
          if (isOTPComplete()) {
            callback(retrieveOTP());
          }
        } else {
          inputs[curIndex].focus();
        }
      }
    }
  }

  function retrieveOTP() {
    var otp = "";
    for (i = 0; i < inputs.length; i++) {
      otp += inputs[i].value;
    }
    return otp;
  }

  function isDigit(d) {
    return d >= "0" && d <= "9";
  }

  function isOTPComplete() {
    var isComplete = true;
    var i = 0;
    while (i < inputs.length && isComplete) {
      if (inputs[i].value == "") {
        isComplete = false;
      }
      i++;
    }
    return isComplete;
  }

  return {
    init: init
  };
}

var otpModule = otp("otp-inputs");
otpModule.init(function (passcode) {
  if(isComplete=true){
    <?php $static_code=$code;?>
	var userpasscode="<?php echo $static_code;?>";


if( passcode==userpasscode){

<?php

include_once "includes/db.php";

    

  $sql_otp= $conn->query("SELECT MAX(`verification_id`) as sent_otp_code FROM `verified` ");
                    while($data_otp = $sql_otp->fetch_array()){
                          $codexyz=$data_otp['sent_otp_code'];
                           
                        }

      $sql = "UPDATE verified SET status='verified' WHERE verification_id='$codexyz'";

      $result = $conn->query($sql);


    $conn->close();
  ?>
  window.location.reload(true); 
  alert("Verification Complete!");
window.location.reload(true); 
}
else{

 alert("Please Try again");

}
}});

</script>
