<?php
include_once "includes/init.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MANGO - Verifier</title>
	<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="assets/css/style.css">
<!-- 	<script src="assets/js/custom.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />


</head>
<body>
	<div class="container">
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img src="assets/images/logo/mangologo.png"</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<?php if(!logged_in()) { ?>
							<li><a href="index.php">Login</a></li>
						<?php }else{ ?>

							<?php  $type = get_usertype($_SESSION['email']);
							echo $type;

							if ($type =='Admin') {
								?>
								<li><a href="superadmin.php">Home</a></li>
								<li><a href="admin.php">Staff page</a></li>
								<li><a href="logout.php">Logout</a></li>
								<?php
							}else{
								?>
								<li><a href="admin.php">Home</a></li>
								<li><a href="logout.php">Logout</a></li>


								<?php
								}
								?>


						<?php }; ?>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
